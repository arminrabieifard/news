<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PanelAsset extends AssetBundle
{
    public $basePath = '@webroot/storage';
    public $baseUrl = '@web/storage';
    public $css = [
        'css/panel/panel.css',
        'css/general/bootstrap-rtl.min.css',
        'css/general/font-awesome.min.css',
        'css/panel/ionicons.min.css',
        'css/panel/AdminLTE.min.css',
        'css/panel/all-skins.min.css' ,
        //'css/all.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\jui\JuiAsset',
    ];
    public $js = [
        'js/general/icheck.min.js',
        'js/general/jquery.slimscroll.min.js',
        'js/general/fastclick.min.js',
        'js/general/myApp.min.js',
        'js/general/demo.js',
        //'js/general/general.js',
    ];
}
