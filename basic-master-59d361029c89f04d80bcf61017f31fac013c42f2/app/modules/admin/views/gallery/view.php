<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'گالری', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
        <div class="gallery-view">

            <p>
                <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('حذف', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    //'category_id',
                    [
                        'attribute' => 'category_id',
                        'format' => 'raw',
                        'value' => $model->category->title,
                    ],
                    'title',
                    'description:ntext',
                    'file_name',
                    'images_file:ntext',
                    'num_view',
                    //'create_at',
                    [
                        'attribute' => 'create_at',
                        'format' => 'raw',
                        'value' => \app\components\General::persianDate($model->create_at),
                    ]
                ],
            ]) ?>

        </div>
    </div>
</div>

