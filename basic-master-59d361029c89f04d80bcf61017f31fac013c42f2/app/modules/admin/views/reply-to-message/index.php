<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ReplyToMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'پاسخ های ارسال شده با ایمیل';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">

        <div class="reply-to-message-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php Pjax::begin(); ?>    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],

                        'id',
                        //'user_message_id',
                        [
                            'attribute' => 'user_message_id',
                            'format' => 'raw',
                            'content' => function($data){
                                return Html::a($data->userMessage->email, ['user-message/view', 'id' => $data->user_message_id]);
                            }
                        ],
                        'reply:ntext',
                        //'create_at',
                        /*[
                            'attribute' => 'create_at',
                            'format' => 'raw',
                            'content' => function($data){
                                return \app\components\General::persianDate($data->create_at);
                            }
                        ],*/
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {delete}'
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
