<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReplyToMessage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'پاسخ به پیام کاربران', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
        <div class="reply-to-message-view">

            <p>
               <!-- <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>-->
                <?= Html::a('حذف', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    //'user_message_id',
                    [
                        'attribute' => 'user_message_id',
                        'format' => 'html',
                        'value' =>
                            Html::a($model->userMessage->email, ['user-message/view', 'id' => $model->user_message_id]),
                    ],
                    [
                        'attribute' => 'null',
                        'label'  => 'پیام',
                        'format' => 'raw',
                        'value' => $model->userMessage->message,
                    ],
                    'reply:ntext',
                    //'create_at',
                    [
                        'attribute' => 'create_at',
                        'format' => 'raw',
                        'value' => \app\components\General::persianDate($model->create_at),
                    ]
                ],
            ]) ?>

        </div>
    </div>
</div>

