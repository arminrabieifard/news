<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model  \app\modules\admin\models\LoginForm*/

$this->title = 'ورود به پنل مدیریت';

?>
<div class="">
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

    <div class="admin-login">
        <div class="login-body">
            <div class="panel panel-default">
                <div class="panel-heading"><h1><?= Html::encode($this->title) ?></h1></div>
                <!-- show summery error-->


                <?php if ($model->errors): ?>
                    <div class="alert alert-danger" role="alert">
                        <strong>خطا!</strong>
                        <?php echo '<br/>'; ?>
                        <?= $form->errorSummary($model); ?>
                    </div>
                <?php endif; ?>
                <div class="panel-body">

                    <div class="input-group">

                        <?= $form->field($model, 'username')->textInput(['placeholder' => 'UserName...'], ['class' => 'form-control'], ['autofocus' => true])->label(false) ?>

                        <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password...'])->label(false) ?>

                        <?= $form->field($model, 'vCode')->widget(\yii\captcha\Captcha::className(), ['captchaAction' => '/site/captcha']) ?>

                    </div>
                    <div>
                        <?= Html::submitButton('ورود', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>




