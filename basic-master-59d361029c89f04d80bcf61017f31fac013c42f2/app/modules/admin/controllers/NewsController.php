<?php

namespace app\modules\admin\controllers;

use app\components\General;
use app\modules\admin\models\ImageUploader;
use Yii;
use app\models\News;
use app\models\NewsSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends CustomController
{
    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $sort = ['id' => SORT_DESC];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $sort);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $imageModel = new ImageUploader();
        $imageModel->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {

            $imageModel->imageFile = UploadedFile::getInstance($imageModel,'imageFile');
            $picName = $imageModel->imageFile->baseName . '_' . Yii::$app->security->generateRandomString(5) . time() . '.' . $imageModel->imageFile->extension;

            if($result = $imageModel->upload($picName, 'news', 700))
            {
                $model->file_name = $picName;
                $model->create_at = time();
                if($model->save())
                {
                    Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
                    return $this->redirect(['site-map/index','section' => 'news', 'page' => 'view' ,'id' => $model->id]);
                }
            }
            else
            {
                Yii::$app->session->setFlash('alert', ['danger', General::showSummaryError($result)]);
                return $this->render('create', [
                    'model' => $model,
                    'imageModel' => $imageModel,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'imageModel' => $imageModel,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $imageModel = new ImageUploader();
        $imageModel->scenario = 'update';

        if(($imageModel->imageFile = UploadedFile::getInstance($imageModel,'imageFile')) !== null)
        {
            $picName = $imageModel->imageFile->baseName . '_' . Yii::$app->security->generateRandomString(5) . time() . '.' . $imageModel->imageFile->extension;
            if($result = $imageModel->updateUpload($picName, 'news', $model->file_name, 700))
            {
                $model->file_name = $picName;
            }
            else
            {
                Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'imageModel' => $imageModel,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        @unlink(Yii::getAlias('@uploads-root') . '/news/' . $model->file_name);
        $model->delete();
        Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
