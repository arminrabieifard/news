<?php

use yii\db\Schema;
use yii\db\Migration;

class m171119_085757_slider extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%slider}}',
            [
                'id'=> $this->primaryKey(10)->unsigned(),
                'title'=> $this->string(255)->null()->defaultValue(null)->comment('عنوان'),
                'file_name'=> $this->string(255)->null()->defaultValue(null)->comment('نام فایل'),
                'visible'=> $this->smallInteger(1)->null()->defaultValue(1)->comment('نمایش داده شود؟'),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%slider}}');
    }
}
