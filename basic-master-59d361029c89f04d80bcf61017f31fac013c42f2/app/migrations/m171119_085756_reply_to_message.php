<?php

use yii\db\Schema;
use yii\db\Migration;

class m171119_085756_reply_to_message extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%reply_to_message}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'user_message_id'=> $this->integer(11)->unsigned()->notNull()->comment('مربوط به پیام'),
                'reply'=> $this->text()->notNull()->comment('متن پاسخ'),
                'visible'=> $this->smallInteger(1)->null()->defaultValue(1)->comment('نمایش داده شود؟'),
                'create_at'=> $this->integer(11)->notNull()->defaultValue(0)->comment('زمان افزودن'),
            ],$tableOptions
        );
        $this->createIndex('idx-reply-user_message_id','{{%reply_to_message}}',['user_message_id'],false);

    }

    public function safeDown()
    {
        $this->dropIndex('idx-reply-user_message_id', '{{%reply_to_message}}');
        $this->dropTable('{{%reply_to_message}}');
    }
}
