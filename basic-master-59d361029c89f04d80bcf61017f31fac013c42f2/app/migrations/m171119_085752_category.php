<?php

use yii\db\Schema;
use yii\db\Migration;

class m171119_085752_category extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%category}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'parent_id'=> $this->integer(11)->unsigned()->null()->defaultValue(null)->comment('زیر شاخه دسته'),
                'title'=> $this->string(50)->notNull()->comment('عنوان'),
                'file_name'=> $this->string(50)->null()->defaultValue(null)->comment('نام فایل'),
                'visible'=> $this->smallInteger(1)->null()->defaultValue(1)->comment('نمایش داده شود؟'),
                'create_at'=> $this->integer(11)->notNull()->defaultValue(0)->comment('زمان افزودن'),
            ],$tableOptions
        );
        $this->createIndex('idx-category-parent_id','{{%category}}',['parent_id'],false);

    }

    public function safeDown()
    {
        $this->dropIndex('idx-category-parent_id', '{{%category}}');
        $this->dropTable('{{%category}}');
    }
}
