<?php

use yii\db\Schema;
use yii\db\Migration;

class m171119_085800_user_message extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%user_message}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'full_name'=> $this->string(255)->notNull()->comment('نام و نام خانوادگی'),
                'email'=> $this->string(255)->notNull()->comment('ایمیل'),
                'message'=> $this->text()->notNull()->comment('متن پیام'),
                'ip'=> $this->string(45)->null()->defaultValue(null)->comment('آی پی'),
                'status'=> $this->smallInteger(1)->unsigned()->null()->defaultValue(0)->comment('وضعیت'),
                'visible'=> $this->smallInteger(1)->null()->defaultValue(1)->comment('نمایش داده شود؟'),
                'create_at'=> $this->integer(11)->notNull()->defaultValue(0)->comment('زمان افزودن'),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%user_message}}');
    }
}
