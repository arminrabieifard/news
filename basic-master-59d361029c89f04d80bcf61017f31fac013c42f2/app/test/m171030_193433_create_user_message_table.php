<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_message`.
 */
class m171030_193433_create_user_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_message', [
            'id'        => $this->primaryKey(11)->unsigned(),
            'full_name' => $this->string(255)->notNull()->comment('نام و نام خانوادگی'),
            'email'     => $this->string(255)->notNull()->comment('ایمیل'),
            'message'   => $this->text()->notNull()->comment('متن پیام'),
            'ip'        => $this->string(45)->null()->comment('آی پی'),
            'status'    => $this->boolean(1)->unsigned()->defaultValue(0)->comment('وضعیت'),
            'visible'     => $this->boolean()->defaultValue(1)->comment('نمایش داده شود؟'),
            'create_at' => $this->integer()->notNull()->defaultValue(0)->comment('زمان افزودن'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_message');
    }
}
