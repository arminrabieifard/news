<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m171030_161126_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id'          => $this->primaryKey(11)->unsigned(),
            'title'       => $this->string(100)->comment('عنوان'),
            'description' => $this->text()->comment('توضیحات'),
            'file_name'   => $this->string(50)->comment('نام فایل'),
            'num_view'    => $this->integer()->defaultValue(0)->comment('تعداد بازدید'),
            'visible'     => $this->boolean()->defaultValue(1)->comment('نمایش داده شود؟'),
            'create_at'   => $this->integer()->notNull()->defaultValue(0)->comment('زمان افزودن'),
        ]);


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
