<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $full_name
 * @property string $user_name
 * @property string $password
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $email
 * @property integer $email_active
 * @property string $email_activation_token
 * @property integer $active
 * @property integer $create_at
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $oldPass;
    public $statusArr = [
    0 => 'غیر فعال',
    1 => 'فعال'
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_name', 'password'], 'required'],
            [['email_active', 'active', 'create_at'], 'integer'],
            [['full_name', 'password_reset_token', 'email_activation_token'], 'string', 'max' => 50],
            [['user_name'], 'string', 'max' => 30],
            [['password', 'email'], 'string', 'max' => 250],
            [['auth_key'], 'string', 'max' => 32],
            [['user_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'نام و نام خانوادگی',
            'user_name' => 'نام کاربری',
            'password' => 'رمز عبور',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'ایمیل',
            'email_active' => 'وضعیت ایمیل',
            'email_activation_token' => 'Email Activation Token',
            'active' => 'وضعیت',
            'create_at' => 'زمان افزودن',
        ];
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * find user by user_name and lower both to don't make mistake
     * @param $userName
     * @return static
     */
    public static function findByUserName($userName)
    {
        return static::findOne(['Lower(user_name)' => strtolower($userName)]);
    }

    /**
     * find user by email
     * @param $email
     * @return static
     */
    public static function findByEmail($email)
    {
        return static::findOne(['LOWER(email)' => strtolower($email)]);
    }


    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getIsNewRecord() == $authKey;
    }

    /**
     * validate entire user password with saved hash password in database
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @param $password
     * @return string
     */
    public function generatePassword()
    {
        $this->password = Yii::$app->security->generatePasswordHash($this->password);
    }

    /**
     * generate auth_key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * generate email Activation token
     */
    public function generateEmailActivationToken()
    {
        $this->email_activation_token = Yii::$app->security->generateRandomString() . '_' . time();
    }


    public function afterFind()
    {
        parent::afterFind();
        $this->oldPass = $this->password;
    }

    public function beforeSave($insert)
    {

        if(trim($this->password) !== '' && $this->password != $this->oldPass)
        {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        }


        if($insert)
        {
            $this->generatePassword();
            $this->generateAuthKey();
            $this->generateEmailActivationToken();
        }
        return parent::beforeSave($insert);
    }
}
