<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reply_to_message".
 *
 * @property string $id
 * @property string $user_message_id
 * @property string $reply
 * @property integer $visible

 * @property integer $create_at
 *
 * @property UserMessage $userMessage
 */
class ReplyToMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reply_to_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_message_id'], 'required'],
            [['user_message_id', 'visible', 'create_at'], 'integer'],
            [['reply'], 'string'],
            [['user_message_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserMessage::className(), 'targetAttribute' => ['user_message_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_message_id' => 'مربوط به پیام',
            'reply' => 'متن پاسخ',
            'visible' => 'نمایش داده شود؟',
            'create_at' => 'زمان افزودن',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserMessage()
    {
        return $this->hasOne(UserMessage::className(), ['id' => 'user_message_id']);
    }
}
