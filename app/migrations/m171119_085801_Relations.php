<?php

use yii\db\Schema;
use yii\db\Migration;

class m171119_085801_Relations extends Migration
{

    public function init()
    {
       $this->db = 'db';
       parent::init();
    }

    public function safeUp()
    {
        $this->addForeignKey('fk_category_parent_id',
            '{{%category}}','parent_id',
            '{{%category}}','id',
            'CASCADE','CASCADE'
         );
        $this->addForeignKey('fk_gallery_category_id',
            '{{%gallery}}','category_id',
            '{{%category}}','id',
            'CASCADE','CASCADE'
         );
        $this->addForeignKey('fk_reply_to_message_user_message_id',
            '{{%reply_to_message}}','user_message_id',
            '{{%user_message}}','id',
            'CASCADE','CASCADE'
         );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_category_parent_id', '{{%category}}');
        $this->dropForeignKey('fk_gallery_category_id', '{{%gallery}}');
        $this->dropForeignKey('fk_reply_to_message_user_message_id', '{{%reply_to_message}}');
    }
}
