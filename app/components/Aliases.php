<?php

namespace app\components;

use Yii;
use yii\base\Component;

class Aliases extends Component{
    public function init() {
        parent::init();

        /** phisical path */
        Yii::setAlias('@storage-root', Yii::getAlias('@webroot') . '/storage');
        Yii::setAlias('@css-root', Yii::getAlias('@webroot') . '/storage/css');
        Yii::setAlias('@js-root', Yii::getAlias('@webroot') . '/storage/js');
        Yii::setAlias('@image-root', Yii::getAlias('@webroot') . '/storage/image');


        Yii::setAlias('@uploads-root', Yii::getAlias('@webroot') . '/uploads');


        /** url-path */
        Yii::setAlias('@storage-url', Yii::$app->request->baseUrl . '/storage');
        Yii::setAlias('@css-url', Yii::$app->request->baseUrl . '/storage/css');
        Yii::setAlias('@js-url', Yii::$app->request->baseUrl . '/storage/js');
        Yii::setAlias('@image-url', Yii::$app->request->baseUrl . '/storage/image');
        Yii::setAlias('@uploads-url', Yii::$app->request->baseUrl . '/uploads');

    }
}