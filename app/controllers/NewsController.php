<?php
/**
 * Created by PhpStorm.
 * User: lvl
 * Date: 11/17/2017
 * Time: 8:18 AM
 */

namespace app\controllers;

use app\models\News;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;

class NewsController extends Controller
{

    public function actionIndex()
    {
        $model = News::find()->where(['visible' => 1]);

        $pages = new Pagination([
            'totalCount' => $model->count(),
            'defaultPageSize' => 20,
        ]);

        $model = $model->offset($pages->offset)->limit($pages->limit)->orderBy(['id' => SORT_DESC])->all();

        return $this->render('index', [
            'model' => $model,
            'pages' => $pages
        ]);
    }
}