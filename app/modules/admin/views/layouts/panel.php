<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\PanelAsset;

PanelAsset::register($this);  // $this represents the view object
/* @var $this yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="@web/js/html5shiv.min.js"></script>
    <script src="@web/js/respond.min.js"></script>
    <![endif]-->
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue fixed sidebar-mini">

<?php $this->beginBody() ?>

<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>P</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Admin</b>Panel</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <!--<li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">4</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">شما 4 پیغام جدید دارید</li>
                            <li>
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <div class="pull-right" style="padding-left: 10px;">
                                                <img src="<?/*= Yii::getAlias('@web') */?>/storage/image/avatar.png" class="img-circle" alt="User Image">
                                            </div>
                                            <h4>
                                                تیم پشتیبانی
                                                <small><i class="fa fa-clock-o"></i> 5 دقیقه</small>
                                            </h4>
                                            <p>بخش فلان کار نمی کند.</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            <li class="footer"><a href="#">نمایش تمام پیغام ها</a></li>
                        </ul>
                    </li>-->
                    <!-- Notifications: style can be found in dropdown.less -->
                    <!--<li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">شما 10 اعلان جدید دارید</li>
                            <li>

                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i>امروز 5 کاربر جدید به سایت اضافه شد.
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">نمایش همه</a></li>
                        </ul>
                    </li>-->
                    <!-- Tasks: style can be found in dropdown.less -->
                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-flag-o"></i>
                            <span class="label label-danger">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">9 وظیفه جدید به شما محول شده است</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                چند عدد دکمه طراحی نمایید
                                                <small class="pull-left">20%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">20% تکمیل شده</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <!-- end task item -->
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="#">نمایش همه وظایف</a>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?= Yii::getAlias('@web') ?>/storage/image/avatar.png" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?= (isset(Yii::$app->user->identity->full_name) ? Yii::$app->user->identity->full_name : 'Developing') ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?= Yii::getAlias('@web') ?>/storage/image/avatar.png" class="img-circle" alt="User Image">

                                <p>
                                    <?= (isset(Yii::$app->user->identity->full_name) ? Yii::$app->user->identity->full_name : 'Developing') ?>
                                    <!--<small>عضو از سال 1395</small>-->
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                               <!-- <div class="row">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">دنبال کننده</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">فروش</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">دوستان</a>
                                    </div>
                                </div>-->
                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="<?= Url::to(['user/view', 'id' => Yii::$app->user->id])?>" class="btn btn-default btn-flat">پروفایل</a>
                                </div>
                                <div class="pull-left">

                                    <?= Html::a('خروج', ['default/logout'],[
                                        'class' => 'btn btn-default btn-flat',
                                        'data-method' => 'POST'])
                                    ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-right image">
                    <img src="<?= Yii::getAlias('@web') ?>/storage/image/avatar.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-right info">
                    <p><?= (isset(Yii::$app->user->identity->full_name) ? Yii::$app->user->identity->full_name : 'Developing') ?></p>
                </div>
            </div>

            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">منوی اصلی</li>
                <li><a href="<?= Url::toRoute(['/admin']) ?>"><i class="fa fa-dashboard"></i><span>پیشخوان</span></a></li>
                <li class="treeview <!--active-->">
                    <a href="<?= Url::toRoute(['slider/index']) ?>">
                        <i class="fa fa-globe"></i>
                        <span>اسلایدر</span>
                        <span class="pull-left-container"><i class="fa fa-angle-left pull-left arrowIcon"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= Url::toRoute(['slider/index']) ?>"><i class="fa fa-circle-o"></i>لیست </a></li>
                        <li><a href="<?= Url::toRoute(['slider/create']) ?>"><i class="fa fa-circle-o"></i>افزودن </a></li>
                    </ul>
                </li>
                <li class="treeview <!--active-->">
                    <a href="<?= Url::toRoute(['user/index']) ?>">
                        <i class="fa fa-globe"></i>
                        <span>ادمین</span>
                        <span class="pull-left-container"><i class="fa fa-angle-left pull-left arrowIcon"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= Url::toRoute(['user/index']) ?>"><i class="fa fa-circle-o"></i>لیست </a></li>
                        <li><a href="<?= Url::toRoute(['user/create']) ?>"><i class="fa fa-circle-o"></i>افزودن </a></li>
                    </ul>
                </li>
                <li class="treeview <!--active-->">
                    <a href="<?= Url::toRoute(['category/index']) ?>">
                        <i class="fa fa-globe"></i>
                        <span>دسته بندی</span>
                        <span class="pull-left-container"><i class="fa fa-angle-left pull-left arrowIcon"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= Url::toRoute(['category/index']) ?>"><i class="fa fa-circle-o"></i>لیست </a></li>
                        <li><a href="<?= Url::toRoute(['category/create']) ?>"><i class="fa fa-circle-o"></i>افزودن </a></li>
                    </ul>
                </li>
                <li class="treeview <!--active-->">
                    <a href="<?= Url::toRoute(['gallery/index']) ?>">
                        <i class="fa fa-globe"></i>
                        <span>گالری</span>
                        <span class="pull-left-container"><i class="fa fa-angle-left pull-left arrowIcon"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= Url::toRoute(['gallery/index']) ?>"><i class="fa fa-circle-o"></i>لیست </a></li>
                        <li><a href="<?= Url::toRoute(['gallery/create']) ?>"><i class="fa fa-circle-o"></i>افزودن </a></li>
                    </ul>
                </li>
                <li class="treeview <!--active-->">
                    <a href="<?= Url::toRoute(['news/index']) ?>">
                        <i class="fa fa-globe"></i>
                        <span>خبر ها</span>
                        <span class="pull-left-container"><i class="fa fa-angle-left pull-left arrowIcon"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= Url::toRoute(['news/index']) ?>"><i class="fa fa-circle-o"></i>لیست </a></li>
                        <li><a href="<?= Url::toRoute(['news/create']) ?>"><i class="fa fa-circle-o"></i>افزودن </a></li>
                    </ul>
                </li>
                <li class="treeview <!--active-->">
                    <a href="<?= Url::toRoute(['tell-us/index']) ?>">
                        <i class="fa fa-globe"></i>
                        <span>تماس با ما</span>
                        <span class="pull-left-container"><i class="fa fa-angle-left pull-left arrowIcon"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= Url::toRoute(['tell-us/index']) ?>"><i class="fa fa-circle-o"></i>لیست </a></li>
                        <li><a href="<?= Url::toRoute(['tell-us/create']) ?>"><i class="fa fa-circle-o"></i>افزودن </a></li>
                    </ul>
                </li>
                <li class="treeview <!--active-->">
                    <a href="<?= Url::toRoute(['about-us/index']) ?>">
                        <i class="fa fa-globe"></i>
                        <span>درباره ما</span>
                        <span class="pull-left-container"><i class="fa fa-angle-left pull-left arrowIcon"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= Url::toRoute(['about-us/index']) ?>"><i class="fa fa-circle-o"></i>لیست </a></li>
                        <li><a href="<?= Url::toRoute(['about-us/create']) ?>"><i class="fa fa-circle-o"></i>افزودن </a></li>
                    </ul>
                </li>
                <li class="treeview <!--active-->">
                    <a href="<?= Url::toRoute(['user-message/index']) ?>">
                        <i class="fa fa-globe"></i>
                        <span>پیغام های کاربران</span>
                        <span class="pull-left-container"><i class="fa fa-angle-left pull-left arrowIcon"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= Url::toRoute(['user-message/index']) ?>"><i class="fa fa-circle-o"></i>لیست </a></li>
                    </ul>
                </li>
                <li class="treeview <!--active-->">
                    <a href="<?= Url::toRoute(['reply-to-message/index']) ?>">
                        <i class="fa fa-globe"></i>
                        <span>لیست پاسخ های ارسالی به کاربران</span>
                        <span class="pull-left-container"><i class="fa fa-angle-left pull-left arrowIcon"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= Url::toRoute(['reply-to-message/index']) ?>"><i class="fa fa-circle-o"></i>لیست </a></li>
                    </ul>
                </li>

                <li class="header">سایر</li>
                <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
                <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <?= \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?php
            if (Yii::$app->session->hasFlash('alert')) {
                list($icon, $message) = Yii::$app->session->getFlash('alert');

                echo \yii\bootstrap\Alert::widget(['options' => [
                    'class' => 'alert-' . $icon,
                ],
                    'body' => $message,
                ]);
            }
            ?>

            <?= $content ?>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->

        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab"></div>
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
