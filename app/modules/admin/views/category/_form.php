<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">

        <div class="category-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?php if(!$model->isNewRecord) :?>

                <?= $form->field($model, 'visible')->checkbox() ?>

            <?php endif;?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'افزودن' : 'ویرایش', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>



            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
