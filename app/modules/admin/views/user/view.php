<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'ادمین', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
        <div class="user-view">

            <p>
                <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('حذف', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'full_name',
                    'user_name',
                    //'password',
                    //'auth_key',
                    //'password_reset_token',
                    'email:email',
                    // 'active',
                    [
                        'attribute' => 'active',
                        'format' => 'raw',
                        'value' => $model->statusArr[$model->active]
                    ],
                    //'create_at',
                    [
                        'attribute' => 'create_at',
                        'format' => 'raw',
                        'value' => \app\components\General::persianDate($model->create_at),
                    ]
                ],
            ]) ?>

        </div>
    </div>
</div>

