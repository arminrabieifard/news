<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\helpers\Url;
use SitemapPHP\Sitemap;
use app\models\Gallery;
use app\models\News;

/**
 * Class SiteMapController
 * @package app\modules\admin\controllers
 */
class SiteMapController extends CustomController
{
    /**
     * @param $section
     * @param $page
     * @param $id
     * @return \yii\web\Response
     */
    public function actionIndex($section, $page, $id)
    {
        $siteMap = new Sitemap(Yii::$app->request->hostInfo);
        $siteMap->setPath(Yii::getAlias('@webroot') . '/');
        $siteMap->addItem('/', '1.0', 'daily', 'Today');
        $siteMap->addItem(Url::to(['/about-us']), '0.8', 'monthly', 'Jun 25');
        $siteMap->addItem(Url::to(['/tell-us']), '0.8', 'yearly', 'Jun 25');


        if(($galleryModel = Gallery::find()->all()) != null)
        {
            foreach ($galleryModel as $item)
            {
                $siteMap->addItem(Url::to(['/gallery/view', 'id' => $item->id]), '0.6', 'weekly', $item->create_at);
            }
        }

        if(($newsModel = News::find()->all()) != null)
        {
            foreach ($newsModel as $item) {
                $siteMap->addItem(Url::to(['/news/view', 'id' => $item->id]), '0.6', 'weekly', $item->create_at);
            }
        }

        $siteMap->createSitemapIndex(Url::base(true) . '/' , 'Today');

        return $this->redirect([$section . '/' . $page, 'id' => $id]);
    }
}