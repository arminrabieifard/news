<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\LoginForm;
use Yii;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends CustomController
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionLogin(){

        $this->layout='login';

        if(!Yii::$app->user->isGuest)
        {
            return $this->redirect(['index']);
        }

        $model = new LoginForm();

        if($model->load(Yii::$app->request->post()) && $model->login())
        {
            return $this->redirect(['index']);
        }
        return $this->render('login',[
            'model' => $model
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
