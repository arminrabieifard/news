<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "about_us".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property integer $create_at
 */
class AboutUs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_us';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['create_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'عنوان',
            'description' => 'متن',
            'create_at' => 'زمان افزودن',
        ];
    }
}
