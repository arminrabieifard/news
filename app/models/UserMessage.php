<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_message".
 *
 * @property string $id
 * @property string $full_name
 * @property string $email
 * @property string $message
 * @property string $ip
 * @property integer $status
 * @property integer $visible

 * @property integer $create_at
 *
 * @property ReplyToMessage[] $replyToMessages
 */
class UserMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'email', 'message'], 'required'],
            [['message'], 'string'],
            [['email'], 'email'],
            [['status', 'visible', 'create_at'], 'integer'],
            [['full_name', 'email'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'نام و نام خانوادگی',
            'email' => 'ایمیل',
            'message' => 'متن پیام',
            'ip' => 'آی پی',
            'status' => 'وضعیت',
            'visible' => 'نمایش داده شود؟',
            'create_at' => 'زمان افزودن',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReplyToMessages()
    {
        return $this->hasMany(ReplyToMessage::className(), ['user_message_id' => 'id']);
    }
}
